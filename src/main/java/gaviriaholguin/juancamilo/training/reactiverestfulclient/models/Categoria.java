package gaviriaholguin.juancamilo.training.reactiverestfulclient.models;

public class Categoria {
    private String id;
    private String nombre;
    
    public String getId() {
        return id;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setId(String id) {
        this.id = id;
    }
    
}
