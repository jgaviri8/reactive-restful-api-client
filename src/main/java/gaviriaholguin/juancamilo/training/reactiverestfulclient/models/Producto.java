package gaviriaholguin.juancamilo.training.reactiverestfulclient.models;

import java.util.Date;

public class Producto {
    private String id;
    private String nombre;
    private Double precio;
    private Date createAt;
    private String foto;
    private Categoria categoria;
    
    public String getId() {
        return id;
    }
    public Categoria getCategoria() {
        return categoria;
    }
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    public String getFoto() {
        return foto;
    }
    public void setFoto(String foto) {
        this.foto = foto;
    }
    public Date getCreateAt() {
        return createAt;
    }
    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }
    public Double getPrecio() {
        return precio;
    }
    public void setPrecio(Double precio) {
        this.precio = precio;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setId(String id) {
        this.id = id;
    }
    @Override
    public String toString() {
        return "Producto [categoria=" + categoria + ", createAt=" + createAt + ", foto=" + foto + ", id=" + id
                + ", nombre=" + nombre + ", precio=" + precio + "]";
    }
    
}
