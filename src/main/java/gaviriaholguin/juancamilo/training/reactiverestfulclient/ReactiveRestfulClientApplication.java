package gaviriaholguin.juancamilo.training.reactiverestfulclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ReactiveRestfulClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactiveRestfulClientApplication.class, args);
	}

}
