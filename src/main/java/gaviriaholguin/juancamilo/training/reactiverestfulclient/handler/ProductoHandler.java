package gaviriaholguin.juancamilo.training.reactiverestfulclient.handler;

import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import gaviriaholguin.juancamilo.training.reactiverestfulclient.models.Producto;
import gaviriaholguin.juancamilo.training.reactiverestfulclient.services.ProductoService;
import reactor.core.publisher.Mono;

@Component
public class ProductoHandler {

    @Autowired
    private ProductoService productoSvc;
    
    public Mono<ServerResponse> listar(ServerRequest request) {
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
          .body(productoSvc.findAll(), Producto.class);
    }

    public Mono<ServerResponse> ver(ServerRequest request) {
        String id = request.pathVariable("id");
        return handleError(productoSvc.findById(id).flatMap(p -> ServerResponse.ok()
          .contentType(MediaType.APPLICATION_JSON)
          .bodyValue(p))
          .switchIfEmpty(ServerResponse.notFound().build()));
    }

    public Mono<ServerResponse> crear(ServerRequest request) {
      Mono<Producto> producto = request.bodyToMono(Producto.class);
      return producto.flatMap(p -> {
        if(p.getCreateAt() == null) {
          p.setCreateAt(new Date());
        }
        return productoSvc.save(p);
      }).flatMap(p -> ServerResponse.created(URI.create("/api/client".concat(p.getId())))
          .contentType(MediaType.APPLICATION_JSON)
          .bodyValue(p))
        .onErrorResume(WebClientResponseException.class, error -> {
          if(error.getStatusCode() == HttpStatus.BAD_REQUEST) {
            return ServerResponse.badRequest().contentType(MediaType.APPLICATION_JSON)
              .bodyValue(error.getResponseBodyAsString());
          }
          return Mono.error(error);
        });
    }

    public Mono<ServerResponse> editar(ServerRequest request) {
      Mono<Producto> producto = request.bodyToMono(Producto.class);
      String id = request.pathVariable("id");
      return handleError(producto.flatMap(p -> productoSvc.update(p, id))
        .flatMap(p -> ServerResponse.created(URI.create("/api/client".concat(p.getId())))
        .contentType(MediaType.APPLICATION_JSON)
        .bodyValue(p)));
    }

    public Mono<ServerResponse> eliminar(ServerRequest request) {
      return handleError(productoSvc.delete(request.pathVariable("id"))
        .then(ServerResponse.noContent().build()));
    }

    public Mono<ServerResponse> upload(ServerRequest request) {
      String id = request.pathVariable("id");
      return handleError(request.multipartData().map(multipart -> multipart.toSingleValueMap().get("file"))
        .cast(FilePart.class)
        .flatMap(file -> productoSvc.upload(file, id))
        .flatMap(p -> ServerResponse.created(URI.create("/api/client".concat(p.getId())))
        .contentType(MediaType.APPLICATION_JSON)
        .bodyValue(p)));
    }

    private Mono<ServerResponse> handleError(Mono<ServerResponse> response) {
      return response.onErrorResume(WebClientResponseException.class, error -> {
        if(error.getStatusCode() == HttpStatus.NOT_FOUND) {
          Map<String, Object> body = new HashMap<>();
          body.put("error", "No existe el producto: ".concat(error.getMessage()));
          body.put("timestamp", new Date());
          body.put("status", error.getStatusCode().value());
          return ServerResponse.status(HttpStatus.NOT_FOUND).bodyValue(body);
        }
        return Mono.error(error);
      });
    }
}
