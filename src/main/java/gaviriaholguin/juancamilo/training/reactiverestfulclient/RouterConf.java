package gaviriaholguin.juancamilo.training.reactiverestfulclient;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import gaviriaholguin.juancamilo.training.reactiverestfulclient.handler.ProductoHandler;

@Configuration
public class RouterConf {
    
    @Bean
    public RouterFunction<ServerResponse> rutas(ProductoHandler handler) {
        return RouterFunctions.route(RequestPredicates.GET("/api/client"), handler::listar)
            .andRoute(RequestPredicates.GET("/api/client/{id}"), handler::ver)
            .andRoute(RequestPredicates.POST("api/client").and(RequestPredicates.contentType(MediaType.APPLICATION_JSON)), handler::crear)
            .andRoute(RequestPredicates.PUT("api/client/{id}").and(RequestPredicates.contentType(MediaType.APPLICATION_JSON)), handler::editar)
            .andRoute(RequestPredicates.DELETE("api/client/{id}"), handler::eliminar)
            .andRoute(RequestPredicates.POST("api/client/upload/{id}"), handler::upload);
    }
}
